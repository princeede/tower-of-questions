import 'package:flutter/material.dart';

import 'homepage.dart';


class LoginPage extends StatelessWidget {
  
  static const PrimaryColor = const Color(0xff622f74);
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(home: Scaffold(
      backgroundColor: Colors.purple[300],
      
      appBar: AppBar(
        title: Text("MADMUC", style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),),
        backgroundColor:  Color(0xff622f74),
        
        
        ),
      body:  Stack( //to add the image on the screen
        children: <Widget>[
          new Container(
            height: 250,
            decoration: new BoxDecoration(image: new DecorationImage(image: new AssetImage("images/home_image.png"), fit: BoxFit.fill)),
          ),
          SingleChildScrollView( //typing unlimited space
          padding: EdgeInsets.all(20.0),
          child: Column(
          children: <Widget>[
          
          Center(
            heightFactor: 4,
            child: Text(
            'Tower of Questions', style: TextStyle(color: Colors.black, fontSize: 40.0, fontWeight: FontWeight.bold) 
          ),
          ),
          SizedBox(height: 40.0,),
          buildTextField("Email"),

          SizedBox(height: 10,),
          buildTextField("Password"),


          SizedBox(height: 20,),
          Container(
            
            child: RaisedButton(
            child: buildButtonContainer(),
            onPressed: (){
              Navigator.push(context, 
                new MaterialPageRoute(builder: (context) => new Homepage()),
              );
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0),
                side: BorderSide(color: Colors.white)
              ),
            // color: Colors.white,

            
            ),
            
          ),
          SizedBox(height: 10),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Forget Password?",
                style: TextStyle(color : Colors.white,),
                
                ),

            ],),
          ),

          // Container(
          
          
          

        ],
      )),
    
        ]),
    ),
    
    );
  }
  
  Widget buildTextField(String hintText){
    
    return TextField(
      
      maxLines: 1,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: TextStyle(color: Colors.grey, fontSize: 25.0, ),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0),
        ),
        prefixIcon: hintText == "Email" ? Icon(Icons.email) : Icon(Icons.lock),
        // suffixIcon: hintText == "Password" ? IconButton(
        //   icon: Icon(Icons.visibility_off), onPressed: (){},): Container()
          )
      );

  }

  Widget buildButtonContainer(){
    return Container(
      height: 60.0,
      width: 120,
      decoration: BoxDecoration(
        borderRadius: (BorderRadius.circular(40.0)),
        // color: Colors.white,
        ),
      child: Center(
        child: Text(
          "Login",
          style: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        ),
      
    );
  }

}