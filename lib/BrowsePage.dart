import 'package:flutter/material.dart';

import 'DrawerClass.dart';
import 'HelpPage.dart';

class BrowsePage extends StatefulWidget {
  @override
  _BrowsePageState createState() => _BrowsePageState();
}

class _BrowsePageState extends State<BrowsePage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
  List<String> nameList = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple[200],
      appBar: AppBar(
        title: Text("Browse", style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),),
        backgroundColor:  Color(0xff622f74),
        
        
      ),
      drawer: DrawerClass(),
      
      body: GestureDetector( 
                    onTap:(){
                      
                      print("Selection: ");
                      // if (index == 2){};
                      Navigator.push(context, 
                        new MaterialPageRoute(builder: (context) => new HelpPage()),
                      );

                    }
                    ,child: Container(
        child: ListWheelScrollView(

          onSelectedItemChanged: (index){
            print("clicked");
            // RaisedButton(onPressed: null,);
            debugPrint("selected item $index");
            if (index == 2){
              print("object");
            }
          },
          
          itemExtent: 100, 
          // useMagnifier: true,
          diameterRatio: 3,
          // offAxisFraction: 0.5,
          magnification: 1.5,




          children: [
            
            // RaisedButton(onPressed: null),
            // InkWell(
            //         onTap:(){
            //           print("Clicked here");

            //         },
            // ListTile(
              
            //   title: Text("data"),
            //   onTap: (){
            //     print("Selection: ");
            //           // if (index == 2){};
            //           Navigator.push(context, 
            //             new MaterialPageRoute(builder: (context) => new HelpPage()),
            //           );
            //   },
            // ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                  child: ListTile(
              
              title: Text("data"),
              onTap: (){
                print("Selection: ");
                      // if (index == 2){};
                      Navigator.push(context, 
                        new MaterialPageRoute(builder: (context) => new HelpPage()),
                      );
              },
            ),
                ),
            ),
            // ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                  child: Center(
                    child: RaisedButton(onPressed: null),)
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0, ),
              child: Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff622f74),
                ),
            ),





          




          ],
          
        ),
        // child:
      ),)
    );
  }
}