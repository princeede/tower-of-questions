import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom_image/pinch_zoom_image.dart';
import 'package:toq_v1/DrawerClass.dart';
import 'package:toq_v1/TopicPage.dart';
// import 'package:toq_v1/TopicScreen.dart';
import 'package:toq_v1/TowerType.dart';
import 'package:toq_v1/loginpage.dart';
import 'package:toq_v1/setting.dart';
// import 'package:zoomable_image/zoomable_image.dart';
// import 'package:photo_view/photo_view.dart';
// import 'package:cached_network_image/cached_network_image.dart';


import 'package:toq_v1/menubar.dart';

class Homepage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    return Scaffold(
      backgroundColor: Colors.purple[200],
      appBar: AppBar(
        
        // iconTheme: new IconThemeData(size: 100, color: Colors.green),
  
        title: Text("Abbey", style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),),
        backgroundColor:  Color(0xff622f74),
        
      ),
      drawer: DrawerClass(),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                "Gems earned: ",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24.0,
                ),
              ),
              ),

          ),
          Container(
            alignment: Alignment(0.8, 1),
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              
              child:RaisedButton(
                child: buildButtonContainer(),
                color: Colors.white,
                // child: Text("Create Tower", style: TextStyle(color: Colors.black, fontSize: 25),),
                onPressed:  (){
                      Navigator.push(context, 
                        new MaterialPageRoute(builder: (context) => new TopicPage()),
                      );
                    },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40.0),
                  side: BorderSide(color:  Color(0xff622f74), width: 2.0 )
                ),
                
                ),

          
              
              ),
          ),
          new Divider(
            height: 20.0,
          ),
          
          Expanded( 
            child: Container(
              color: Colors.black,
            // alignment: Alignment.topCenter,
            
              child: _gridList(),
              
            //  Image(
            //   // width: 400.0,
            //   // height: 60.0,
            //   fit: BoxFit.cover,
              
            //   image: NetworkImage('https://www.w3schools.com/w3css/img_lights.jpg', ),
            //   ),
          ),
          ),

        ],
        


        
      ),
    );
  
  }

  Widget buildButtonContainer(){
    return Container(
      height: 60.0,
      width: 140,
      decoration: BoxDecoration(
        borderRadius: (BorderRadius.circular(40.0)),
        color: Colors.white,
        ),
      child: Center(
        child: Text(
          "Create Tower",
          style: TextStyle(color: Colors.black, fontSize: 22.0, fontWeight: FontWeight.bold),
        ),
        ),
      
    );
  }
  Widget _gridList() {
    return GridView.builder(
      itemCount: 64,
      gridDelegate:
      SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 8),
      itemBuilder: (context, index) {
        return Padding(
          
          padding: const EdgeInsets.all(0.0),
          child: GestureDetector(
            
            child:  
            // PinchZoomImage(
            //   image:
              new Image.asset(              
              'Assets/Map/map_$index.png',
              // height: 100 ,            
              ),
              // onZoomStart: () {
              //     print('Zoom started');
              //   },
              //   onZoomEnd: () {
              //     print('Zoom finished');
              //   },
              // ),
              
            onTap: () {
              _openImage(context, index);
            },
          ),
        );
      },
    );
  }
  _openImage(context, index) async {
    showDialog(
      context: context,
      builder: (a) => AlertDialog(
        title: Text("Location Details"),
        content: new Image.asset(
          'Assets/Map/map_$index.png',
          
        ),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                _dismissDialog(context);
              },
              child: Text('Close'))
        ],
      ),
    );
  }
   _dismissDialog(context) {
    Navigator.pop(context);
  }
}

